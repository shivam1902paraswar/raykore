const axios = require('axios');


exports.jokes = async (req, res) => {
    try {
        const { count } = req.query
        let responseData = [];
        for (let i = 0; i < count; i++) {
            const randomJokes = await axios.get('https://api.chucknorris.io/jokes/random');
            const { id, value } = randomJokes.data;
            responseData.push({ id, jokeText: value })
        }
        res.status(200).send(responseData)
    } catch (err) {
        res.status(err.response.data.status ? err.response.data.status : 500).send(err.response.data.message ? err.response.data.message : "Something Went Wrong")
    }
}