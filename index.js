const express = require('express');
const cors = require('cors');

const { jokes } = require('./API/jokes');

const app = express();
app.use(cors());
app.get('/api/jokes', jokes)

app.listen(3050, () => {
    console.log(`Example app listening on port 3050`)
})
